import React from 'react';
import PropTypes from 'prop-types';
import {
  Route,
  Redirect,
  withRouter,
} from 'react-router-dom';

const AuthorizedRouteComponent = (props) => {
  const {
    token,
    component,
    exact,
    path,
  } = props;

  if (token) {
    return (
      <Route
        exact={exact}
        component={component}
        path={path}
      />
    );
  }

  return <Redirect to="/" />;
};

AuthorizedRouteComponent.defaultProps = {
  exact: false,
  token: null,
};

AuthorizedRouteComponent.propTypes = {
  component: PropTypes.func.isRequired,
  exact: PropTypes.bool,
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
  path: PropTypes.string.isRequired,
  token: PropTypes.string,
};

export default withRouter(AuthorizedRouteComponent);
