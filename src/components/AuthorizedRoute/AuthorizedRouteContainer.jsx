import { connect } from 'react-redux';
import { selectToken } from '../../resources/security';
import Component from './AuthorizedRouteComponent';

const mapStateToProps = (state) => {
  const token = selectToken(state);

  return ({
    token: token ? token.data : null,
  });
};

export default connect(mapStateToProps)(Component);
