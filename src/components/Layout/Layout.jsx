import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MessageIcon from '@material-ui/icons/Message';
import PeopleIcon from '@material-ui/icons/People';
import PersonIcon from '@material-ui/icons/Person';
import SettingsIcon from '@material-ui/icons/Settings';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { selectLoggedUser } from '../../resources/security';
import { removeApplicationStore } from '../../services/storageService';
import history from '../../history';

const styles = {
  content: {
    margin: '2.5rem',
  },
  fullList: {
    width: 'auto',
  },
  grow: {
    flexGrow: 1,
  },
  list: {
    width: 250,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  root: {
    flexGrow: 1,
  },
};

const Layout = ({ classes, children, loggedUser }) => {
  const [auth] = useState(true);
  const [anchorEl, setAnchorEl] = useState(null);
  const [drawerOpen, setDrawerOpen] = useState(false);

  const open = Boolean(anchorEl);
  const sideList = (

    <div className={classes.list}>
      <List>
        <ListItem
          button
          onClick={() => {
            history.push('/message');
          }}
        >
          <ListItemIcon>
            {' '}
            <MessageIcon />
            {' '}
          </ListItemIcon>
          <ListItemText primary="SMS" />
        </ListItem>
        <ListItem
          button
          onClick={() => {
            history.push('/contacts');
          }}
        >
          <ListItemIcon>
            {' '}
            <PersonIcon />
            {' '}
          </ListItemIcon>
          <ListItemText primary="Contacts" />
        </ListItem>
        <ListItem
          button
          onClick={() => {
            history.push('/contactGroup');
          }}
        >
          <ListItemIcon>
            {' '}
            <PeopleIcon />
            {' '}
          </ListItemIcon>
          <ListItemText primary="Contact Groups" />
        </ListItem>
      </List>
      <Divider />
      <List>
        <ListItem
          button
          onClick={() => {
            history.push('/users');
          }}
        >
          <ListItemIcon>
            {' '}
            <SettingsIcon />
            {' '}
          </ListItemIcon>
          <ListItemText primary="Users" />
        </ListItem>
      </List>
    </div>
  );

  return (
    <div className={classes.root}>
      <Drawer
        open={drawerOpen}
        onClose={() => {
          setDrawerOpen(false);
        }}
      >
        <div
          tabIndex={0}
          role="button"
          onClick={() => {
            setDrawerOpen(false);
          }}
          onKeyDown={() => {
            setDrawerOpen(false);
          }}
        >
          {sideList}
        </div>
      </Drawer>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            className={classes.menuButton}
            onClick={() => {
              setDrawerOpen(true);
            }}
            color="inherit"
            aria-label="Menu"
          >
            <MenuIcon />

          </IconButton>
          <Typography variant="h6" color="inherit" className={classes.grow}>
                        GSM Gate
          </Typography>
          {
                        `${loggedUser.firstName} ${loggedUser.lastName}`
                    }

          {auth && (
            <div>
              <IconButton
                aria-owns={open ? 'menu-appbar' : undefined}
                aria-haspopup="true"
                onClick={(event) => {
                  setAnchorEl(event.currentTarget);
                }}
                color="inherit"
              >
                <AccountCircle />
              </IconButton>

              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  horizontal: 'right',
                  vertical: 'top',
                }}
                transformOrigin={{
                  horizontal: 'right',
                  vertical: 'top',
                }}
                open={open}
                onClose={() => {
                  setAnchorEl(null);
                }}
              >
                <MenuItem onClick={() => {
                  removeApplicationStore();
                  window.location = '/';
                }}
                >
                    Logout
                </MenuItem>
              </Menu>
            </div>
          )}
        </Toolbar>
      </AppBar>
      <div className={classes.content}>
        {children}
      </div>
    </div>
  );
}

Layout.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  const loggedUser = selectLoggedUser(state);

  return ({
    loggedUser: loggedUser.data,
  });
};

export default connect(
  mapStateToProps,
)(withRouter(withStyles(styles)(Layout)));
