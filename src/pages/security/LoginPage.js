import React, {
  useState, useEffect,
} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import validate from '../../validation/login/LoginFormValidationRules';
import {
  logInUser,
  selectLoggedUser,
} from '../../resources/security/index';
import history from '../../history';

const styles = theme => ({
  avatar: {
    backgroundColor: theme.palette.secondary.main,
    margin: theme.spacing.unit,

  },
  form: {
    marginTop: theme.spacing.unit,
    width: '100%', // Fix IE 11 issue.

  },
  main: {
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    width: 'auto',
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      marginLeft: 'auto',
      marginRight: 'auto',
      width: 400,
    },
  },
  paper: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    marginTop: theme.spacing.unit * 8,
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
  validation: {
    color: 'red',
  },
});

function SignIn({
  classes, loggedUser, logInUser,
}) {
  useEffect(() => {
    if (loggedUser) {
      history.push('/message/');
    }
  });

  const [errors, setErrors] = useState({});
  const [values, setValues] = useState({});

  const handleChange = (event) => {
    event.persist();
    setValues(values => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    e.stopPropagation();
    setErrors(validate(values));
    logInUser(values);
  };

  return (
    <div className={classes.main}>
      <CssBaseline />
      <Paper className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
                    Sign in
        </Typography>
        <form
          className={classes.form}
          onSubmit={handleSubmit}
        >
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="email">Email Address</InputLabel>
            <Input
              id="email"
              className={`input ${errors.email && 'is-danger'}`}
              onChange={handleChange}
              name="email"
              value={values.email || ''}
              autoComplete="email"
              autoFocus
            />
            {errors.email && (
            <p className={classes.validation}>{errors.email}</p>
            )}
          </FormControl>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="password">Password</InputLabel>
            <Input
              name="password"
              className={`input ${errors.password && 'is-danger'}`}
              value={values.password || ''}
              onChange={handleChange}
              type="password"
              id="password"
              autoComplete="current-password"
            />
            {errors.password && (
            <p className={classes.validation}>{errors.password}</p>
            )}
          </FormControl>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
                        Sign in
          </Button>
        </form>
      </Paper>
    </div>
  );
}
SignIn.propTypes = {
  logInUser: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  const loggedUser = selectLoggedUser(state);

  return ({
    isPosting: loggedUser.isPosting,
    loggedUser: loggedUser.data,
  });
};

const mapDispatchToProps = dispatch => ({
  logInUser: values => dispatch(logInUser(values)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(withStyles(styles)(SignIn)));
