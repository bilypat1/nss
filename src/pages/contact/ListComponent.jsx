import { withRouter } from 'react-router';
import withStyles from '@material-ui/core/styles/withStyles';
import React, {
  useEffect, useState,
} from 'react';
import Chip from '@material-ui/core/Chip';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Layout from '../../components/Layout/Layout';
import { AddContactModal } from './components/AddContactModal';
import { UpdateContactModal } from './components/UpdateContactModal';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
    flexGrow: 1,
  },
  paper: {
    boxSizing: 'border-box',
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
    width: '100%',
    margin: '2.5rem 0',
    overflowX: 'scroll',
  },
  table: {
    minWidth: '768px',
  },
  chip: {
    margin: theme.spacing.unit,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  button: {
    marginBottom: '2.5rem',
  },
});

function List({
  classes, loggedUser, token, fetchPublicGroups, fetchPrivateGroups, group, privateGroup, addContact, editContact, fetchPublicContacts, publicContacts, fetchPrivateContacts, privateContacts, deleteContact,
}) {
  const [createContact, setCreateContact] = useState(null);
  const [updateContact, setUpdateContact] = useState(null);

  useEffect(() => {
    if (!group || !privateGroup || !publicContacts || !privateContacts) {
      fetchPublicGroups();
      fetchPrivateGroups(loggedUser.id);
      fetchPublicContacts();
      fetchPrivateContacts(loggedUser.id);
    }
  });

  const groups = group || [];
  const privateGroups = privateGroup || [];
  const publicContact = publicContacts || [];
  const privateContact = privateContacts || [];

  const handleSubmit = (values, contactGroups, owned) => {
    addContact(values, contactGroups, owned ? loggedUser.id : null, loggedUser.id);
  };

  const onCreateOpen = () => {
    setCreateContact(<AddContactModal open close={() => { setCreateContact(null); }} privateGroup={privateGroups} onSubmit={handleSubmit} publicGroup={groups} />);
  };
  const onUpdateOpen = (id, row) => {
    setUpdateContact(<UpdateContactModal id={id} open oldData={row} close={() => { setUpdateContact(null); }} onSubmit={onUpdateContactSubmit} />);
  };

  const onUpdateContactSubmit = (id, name, phoneNumber) => {
    editContact(id, name, phoneNumber, loggedUser.id);
  };

  const onDeleteContact = (id) => {
    deleteContact(id, loggedUser.id);
  };

  const renderContactGroups = (contactGroupId) => {
    const mergedGroups = [
      ...group,
      ...privateGroup,
    ];

    return (
      <Chip
        className={classes.chip}
        label={mergedGroups.find(mergedGroup => mergedGroup.id === contactGroupId).name}
      />
    );
  };

  return (
    <Layout>
      <Button variant="contained" color="primary" className={classes.button} onClick={() => { onCreateOpen(); }}>
                Create new contact
      </Button>
      <div>
        <Typography component="h1" variant="h5">
                    Public contacts
        </Typography>
        <Paper className={classes.paper}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell align="right">Phone number</TableCell>
                <TableCell align="right">Contact groups</TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {publicContact.map(row => (
                <TableRow key={row.id}>
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell align="right">{row.phoneNumber}</TableCell>
                  <TableCell align="right">{row.contactGroups && row.contactGroups.map(renderContactGroups)}</TableCell>
                  <TableCell align="right">
                    <Button onClick={() => { onUpdateOpen(row.id, row); }}>Update</Button>
                    <Button color="secondary" onClick={() => { onDeleteContact(row.id); }}>Delete</Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
      </div>
      <div>
        <Typography component="h1" variant="h5">
                    Private contacts
        </Typography>
        <Paper className={classes.paper}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell align="right">Phone number</TableCell>
                <TableCell align="right">Contact groups</TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {privateContact.map(row => (
                <TableRow key={row.id}>
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell align="right">{row.phoneNumber}</TableCell>
                  <TableCell align="right">{row.contactGroups && row.contactGroups.map(renderContactGroups)}</TableCell>
                  <TableCell align="right">
                    <Button onClick={() => { onUpdateOpen(row.id, row); }}>Update</Button>
                    <Button color="secondary" onClick={() => { onDeleteContact(row.id); }}>Delete</Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
      </div>
      {createContact}
      {updateContact}
    </Layout>
  );
}

export default withRouter(withStyles(styles)(List));
