import Dialog from '@material-ui/core/Dialog/index';
import withStyles from '@material-ui/core/styles/withStyles';
import DialogTitle from '@material-ui/core/DialogTitle/index';
import DialogContent from '@material-ui/core/DialogContent/index';
import DialogContentText from '@material-ui/core/DialogContentText/index';
import FormControl from '@material-ui/core/FormControl/index';
import InputLabel from '@material-ui/core/InputLabel/index';
import Input from '@material-ui/core/Input/index';
import Select from '@material-ui/core/Select/index';
import MenuItem from '@material-ui/core/MenuItem/index';
import Button from '@material-ui/core/Button/index';
import React, { useState } from 'react';
import DialogActions from '@material-ui/core/DialogActions/index';
import Checkbox from '@material-ui/core/Checkbox/index';
import FormControlLabel from '@material-ui/core/FormControlLabel/index';
import validate from '../../../../validation/contact/CreateFormValidationRules';

const styles = () => ({
  validation: {
    color: 'red',
  },
}
);

function AddContactModal({
  open, close, onSubmit, privateGroup, publicGroup, classes,
}) {
  const [contactGroups, setContactGroups] = useState([]);
  const [errors, setErrors] = useState({});
  const [owned, setOwner] = useState(false);
  const [values, setValues] = useState({});

  const handleChange = (event) => {
    event.persist();
    setValues(values => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
  };

  const handleSelectChange = (event) => {
    setContactGroups(event.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const errs = validate(values);
    setErrors(errs);
    if (Object.keys(errs).length === 0) {
      onSubmit(values, contactGroups, owned);
      close();
    }
  };

  return (
    <Dialog
      open={open}
      aria-labelledby="form-dialog-title"
      onClose={() => close()}
    >
      <DialogTitle id="form-dialog-title">Create new contact</DialogTitle>
      <DialogContent>
        <DialogContentText>
                    This form is use for creating new contact.
        </DialogContentText>
        <form
          onSubmit={handleSubmit}
        >
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="name">Contact name</InputLabel>
            <Input
              id="name"
              className={`input ${errors.name && 'is-danger'}`}
              name="name"
              onChange={handleChange}
              value={values.name || ''}
              required
              autoFocus
            />
            {errors.name && (
              <p className={classes.validation}>{errors.name}</p>
            )}
          </FormControl>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="phoneNumber">Contact phone number</InputLabel>
            <Input
              id="phoneNumber"
              name="phoneNumber"
              className={`input ${errors.phoneNumber && 'is-danger'}`}
              onChange={handleChange}
              value={values.phoneNumber || ''}
              required
            />
            {errors.phoneNumber && (
              <p className={classes.validation}>{errors.phoneNumber}</p>
            )}
          </FormControl>
          <FormControlLabel
            control={(
              <Checkbox
                id="checkMe"
                name="checkMe"
                onChange={() => {
                  setOwner(!owned);
                }}
                value={owned}
              />
)}
            label="Private contact"
          />
          <InputLabel htmlFor="select-multiple">Add to contact groups   </InputLabel>
          <Select
            multiple
            value={contactGroups}
            onChange={handleSelectChange}
            input={<Input id="select-multiple" />}
          >
            {(owned ? privateGroup : publicGroup).map(group => (
              <MenuItem key={group.name} value={group.id}>
                {group.name}
              </MenuItem>
            ))}
          </Select>
        </form>
      </DialogContent>

      <DialogActions>
        <Button color="primary" onClick={() => close()}>
                    Cancel
        </Button>
        <Button
          type="submit"
          color="primary"
          onClick={event => handleSubmit(event)}
        >
                    Create
        </Button>
      </DialogActions>
    </Dialog>

  );
}

export default withStyles(styles)(AddContactModal);
