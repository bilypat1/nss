import DialogTitle from '@material-ui/core/DialogTitle/index';
import DialogContent from '@material-ui/core/DialogContent/index';
import DialogContentText from '@material-ui/core/DialogContentText/index';
import FormControl from '@material-ui/core/FormControl/index';
import InputLabel from '@material-ui/core/InputLabel/index';
import Input from '@material-ui/core/Input/index';
import DialogActions from '@material-ui/core/DialogActions/index';
import Button from '@material-ui/core/Button/index';
import Dialog from '@material-ui/core/Dialog/index';
import withStyles from '@material-ui/core/styles/withStyles';
import React, { useState } from 'react';
import validate from '../../../../validation/contact/UpdateFormValidationRules';

const styles = () => ({
  validation: {
    color: 'red',
  },
}
);

function UpdateContactModal({
  open, close, oldData, id, onSubmit, classes,
}) {
  const [name, setOldName] = useState(oldData.name);
  const [phone, setOldPhone] = useState(oldData.phoneNumber);
  const [errors, setErrors] = useState({});
  const [values, setValues] = useState({});

  const handleSubmit = (e) => {
    e.preventDefault();
    e.stopPropagation();
    setValues({
      name,
      phoneNumber: phone,
    });
    const err = validate(values);
    setErrors(err);
    if (Object.keys(err).length === 0) {
      onSubmit(id, values);
      close();
    }
  };
  return (
    <Dialog
      open={open}
      aria-labelledby="form-dialog-title"
      onClose={() => close()}
    >
      <DialogTitle id="form-dialog-title">Update contact</DialogTitle>
      <DialogContent>
        <DialogContentText>
                    This form is use for updating contact.
        </DialogContentText>
        <form
          onSubmit={handleSubmit}
        >
          <FormControl margin="normal" fullWidth>
            <InputLabel htmlFor="name">Contact name</InputLabel>
            <Input
              id="name"
              value={name}
              onChange={(event) => {
                setOldName(event.target.value);
              }}
              name="name"
            />
            {errors.name && (
            <p className={classes.validation}>{errors.name}</p>
            )}
          </FormControl>
          <FormControl margin="normal" fullWidth>
            <InputLabel htmlFor="phone">Contact phone number</InputLabel>
            <Input
              id="phoneNumber"
              onChange={(event) => {
                setOldPhone(event.target.value);
              }}
              name="phoneNumber"
              value={phone}
            />
            {errors.phoneNumber && (
            <p className={classes.validation}>{errors.phoneNumber}</p>
            )}
          </FormControl>
        </form>
      </DialogContent>
      <DialogActions>
        <Button color="primary" onClick={() => close()}>
                    Cancel
        </Button>
        <Button
          type="submit"
          color="primary"
          onClick={event => handleSubmit(event)}
        >
                    Create
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default withStyles(styles)(UpdateContactModal);

