import { connect } from 'react-redux';
import {
  selectLoggedUser,
  selectToken,
} from '../../resources/security';
import {
  fetchPrivateGroups,
  fetchPublicGroups,
  selectPublicGroups,
  selectPrivateGroups,
} from '../../resources/contactGroup';
import {
  addContact,
  deleteContact,
  editContact,
  fetchPrivateContacts,
  fetchPublicContacts,
  selectPrivateContacts,
  selectPublicContacts,
} from '../../resources/contact';
import Component from './ListComponent';

const mapStateToProps = (state) => {
  const loggedUser = selectLoggedUser(state);
  const group = selectPublicGroups(state);
  const token = selectToken(state);
  const privateGroup = selectPrivateGroups(state);
  const publicContacts = selectPublicContacts(state);
  const privateContacts = selectPrivateContacts(state);

  return ({
    group: group.data,
    loggedUser: loggedUser.data,
    privateContacts: privateContacts.data,
    privateGroup: privateGroup.data,
    publicContacts: publicContacts.data,
    token: token.data,
  });
};

const mapDispatchToProps = dispatch => ({
  addContact: (values, contactGroup, user, actualUser) => dispatch(
    addContact(values, contactGroup, user, actualUser),
  ),
  deleteContact: (id, user) => dispatch(deleteContact(id, user)),
  editContact: (id, values, user) => dispatch(
    editContact(id, values, user),
  ),
  fetchPrivateContacts: id => dispatch(fetchPrivateContacts(id)),
  fetchPrivateGroups: id => dispatch(fetchPrivateGroups(id)),
  fetchPublicContacts: () => dispatch(fetchPublicContacts()),
  fetchPublicGroups: () => dispatch(fetchPublicGroups()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Component);
