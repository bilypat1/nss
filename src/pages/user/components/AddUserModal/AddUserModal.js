import FormControl from '@material-ui/core/FormControl/index';
import InputLabel from '@material-ui/core/InputLabel/index';
import Input from '@material-ui/core/Input/index';
import withStyles from '@material-ui/core/styles/withStyles';
import FormControlLabel from '@material-ui/core/FormControlLabel/index';
import Checkbox from '@material-ui/core/Checkbox/index';
import Button from '@material-ui/core/Button/index';
import React, { useState } from 'react';
import Dialog from '@material-ui/core/Dialog/index';
import DialogTitle from '@material-ui/core/DialogTitle/index';
import DialogContent from '@material-ui/core/DialogContent/index';
import DialogContentText from '@material-ui/core/DialogContentText/index';
import DialogActions from '@material-ui/core/DialogActions/index';
import validate from '../../../../validation/user/CreateFormValidationRules';

const styles = () => ({
  validation: {
    color: 'red',
  },
}
);

function AddUserModal({
  open, close, onSubmit, classes,
}) {
  const [admin, setAdmin] = useState(false);
  const [values, setValues] = useState({});
  const [errors, setErrors] = useState({});

  const handleChange = (event) => {
    event.persist();
    setValues(values => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const err = validate(values);
    setErrors(err);
    if (Object.keys(err).length === 0) {
      onSubmit(values, admin);
      close();
    }
  };

  return (
    <Dialog
      open={open}
      aria-labelledby="form-dialog-title"
      onClose={() => close()}
    >
      <DialogTitle id="form-dialog-title">Create new user</DialogTitle>
      <DialogContent>
        <DialogContentText>
                    This form is use for creating new user.
        </DialogContentText>
        <form
          onSubmit={handleSubmit}
        >
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="email">User email</InputLabel>
            <Input
              id="email"
              name="email"
              onChange={handleChange}
              required
              autoComplete="email"
              autoFocus
            />
            {errors.email && (
              <p className={classes.validation}>{errors.email}</p>
            )}
          </FormControl>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="firstName">First name</InputLabel>
            <Input
              id="firstName"
              name="firstName"
              onChange={handleChange}
              required
            />
            {errors.firstName && (
              <p className={classes.validation}>{errors.firstName}</p>
            )}
          </FormControl>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="lastName">Last name</InputLabel>
            <Input
              id="lastName"
              name="lastName"
              onChange={handleChange}
              required
            />
            {errors.lastName && (
              <p className={classes.validation}>{errors.lastName}</p>
            )}
          </FormControl>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="password">Password</InputLabel>
            <Input
              id="password"
              name="password"
              onChange={handleChange}
              required
              type="password"
            />
            {errors.password && (
              <p className={classes.validation}>{errors.password}</p>
            )}
          </FormControl>
          <FormControlLabel
            control={(
              <Checkbox
                id="checkMe"
                name="checkMe"
                onChange={() => {
                  setAdmin(!admin);
                }}
                value={admin}
              />
)}
            label="Admin "
          />
        </form>
      </DialogContent>

      <DialogActions>
        <Button color="primary" onClick={() => close()}>
                    Cancel
        </Button>
        <Button
          type="submit"
          color="primary"
          onClick={event => handleSubmit(event)}
        >
                    Create
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default withStyles(styles)(AddUserModal);
