import DialogTitle from '@material-ui/core/DialogTitle/index';
import DialogContent from '@material-ui/core/DialogContent/index';
import DialogContentText from '@material-ui/core/DialogContentText/index';
import FormControl from '@material-ui/core/FormControl/index';
import InputLabel from '@material-ui/core/InputLabel/index';
import Input from '@material-ui/core/Input/index';
import DialogActions from '@material-ui/core/DialogActions/index';
import Button from '@material-ui/core/Button/index';
import Dialog from '@material-ui/core/Dialog/index';
import React, { useState } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import validate from '../../../../validation/user/UpdateFormValidationRules';

const styles = () => ({
  validation: {
    color: 'red',
  },
}
);

function UpdateContactModal({
  open, close, oldData, onSubmit, classes
}) {
  const [email, setEmail] = useState(oldData.email);
  const [firstName, setFirstName] = useState(oldData.firstName);
  const [lastName, setLastName] = useState(oldData.lastName);
  const [errors, setErrors] = useState({});
  const [values, setValues] = useState({});

  const handleSubmit = (e) => {
    e.preventDefault();
    e.stopPropagation();
    setValues({
      email,
      firstName,
      lastName,
    });
    const err = validate(values);
    setErrors(err);
    if (Object.keys(err).length === 0) {
      onSubmit(oldData.id, values, oldData.role);
      close();
    }
  };

  return (
    <Dialog
      open={open}
      aria-labelledby="form-dialog-title"
      onClose={() => close()}
    >
      <DialogTitle id="form-dialog-title">Update new contact group</DialogTitle>
      <DialogContent>
        <DialogContentText>
                    This form is use for updating new contact group.
        </DialogContentText>
        <form
          onSubmit={handleSubmit}
        >
          <FormControl margin="normal" fullWidth>
            <InputLabel htmlFor="email">Email</InputLabel>
            <Input
              id="email"
              onChange={(event) => {
                setEmail(event.target.value);
              }}
              name="email"
              value={email}
              autoFocus
            />
            {errors.email && (
            <p className={classes.validation}>{errors.email}</p>
            )}
          </FormControl>
          <FormControl margin="normal" fullWidth>
            <InputLabel htmlFor="name">First name</InputLabel>
            <Input
              id="firstName"
              onChange={(event) => {
                setFirstName(event.target.value);
              }}
              name="firstName"
              value={firstName}
            />
            {errors.firstName && (
            <p className={classes.validation}>{errors.firstName}</p>
            )}
          </FormControl>
          <FormControl margin="normal" fullWidth>
            <InputLabel htmlFor="name">Last name</InputLabel>
            <Input
              id="lastName"
              onChange={(event) => {
                setLastName(event.target.value);
              }}
              name="lastName"
              value={lastName}
            />
            {errors.lastName && (
            <p className={classes.validation}>{errors.lastName}</p>
            )}
          </FormControl>
        </form>
      </DialogContent>
      <DialogActions>
        <Button color="primary" onClick={() => close()}>
                    Cancel
        </Button>
        <Button
          type="submit"
          color="primary"
          onClick={event => handleSubmit(event)}
        >
                    Update
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default withStyles(styles)(UpdateContactModal);

