import { withRouter } from 'react-router';
import withStyles from '@material-ui/core/styles/withStyles';
import React, {
  useEffect, useState,
} from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Layout from '../../components/Layout/Layout';
import { AddUserModal } from './components/AddUserModal';
import { UpdateUserModal } from './components/UpdateUserModal';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
    flexGrow: 1,
  },
  paper: {
    boxSizing: 'border-box',
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
    width: '100%',
    margin: '2.5rem 0',
    overflowX: 'scroll',
  },
  table: {
    minWidth: '576px',
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  button: {
    marginBottom: '2.5rem',
  },
});

function ListUsers({
  classes, loggedUser, token, publicUser, fetchPublicUsers, addUser, editUser, deleteUser,
}) {
  const [createUser, setCreateUser] = useState(false);
  const [canEdit, setEdit] = useState(false);
  const [updateContact, seUpdateContact] = useState(false);

  useEffect(() => {
    if (!publicUser) {
      fetchPublicUsers();
    }
  });

  const users = publicUser || [];

  const onCreateOpen = () => {
    setCreateUser(<AddUserModal open close={() => { setCreateUser(null); }} onSubmit={handleSubmit} />);
  };

  const handleSubmit = (email, firstName, lastName, password, admin) => {
    addUser(email, firstName, lastName, password, admin ? 'ROLE_ADMIN' : 'ROLE_USER');
  };

  const onUpdateOpen = (row) => {
    seUpdateContact(<UpdateUserModal open close={() => { seUpdateContact(null); }} oldData={row} onSubmit={onUpdateUserSubmit} />);
  };

  const onUpdateUserSubmit = (id, email, firstName, lastName, role) => {
    editUser(id, email, firstName, lastName, role);
  };
  const onDeleteUser = (id) => {
    deleteUser(id);
  };

  return (
    <Layout>
      <Button variant="contained" color="primary" className={classes.button} onClick={() => { onCreateOpen(); }}>
                Create new user
      </Button>
      <div>
        <Typography component="h1" variant="h5">
                    Users
        </Typography>
        {updateContact}
        <Paper className={classes.paper}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell>Email</TableCell>
                <TableCell>Name </TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {users.map(row => (
                <TableRow key={row.id}>
                  <TableCell scope="row">
                    {row.email}
                  </TableCell>
                  <TableCell scope="row">
                    {`${row.firstName} ${row.lastName}`}
                  </TableCell>
                  <TableCell align="right">
                    <Button onClick={() => { onUpdateOpen(row); }}>Update</Button>
                    <Button color="secondary" onClick={() => { onDeleteUser(row.id); }}>Delete</Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
      </div>
      {createUser}
    </Layout>
  );
}

export default withRouter(withStyles(styles)(ListUsers));
