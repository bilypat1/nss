import { connect } from 'react-redux';
import {
  fetchPublicUsers,
  addUser,
  editUser,
  deleteUser,
  selectPublicUser,
} from '../../resources/user/index';
import {
  selectLoggedUser,
  selectToken,
} from '../../resources/security';
import Component from './ListComponent';

const mapStateToProps = (state) => {
  const loggedUser = selectLoggedUser(state);
  const token = selectToken(state);
  const publicUser = selectPublicUser(state);

  return ({
    loggedUser: loggedUser.data,
    publicUser: publicUser.data,
    token: token.data,
  });
};

const mapDispatchToProps = dispatch => ({
  addUser: (values, role) => dispatch(
    addUser(values, role),
  ),
  deleteUser: id => dispatch(deleteUser(id)),
  editUser: (id, values, role) => dispatch(
    editUser(id, values, role),
  ),
  fetchPublicUsers: () => dispatch(fetchPublicUsers()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Component);
