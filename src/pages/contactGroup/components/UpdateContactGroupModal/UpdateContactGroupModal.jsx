import DialogTitle from '@material-ui/core/DialogTitle/index';
import DialogContent from '@material-ui/core/DialogContent/index';
import DialogContentText from '@material-ui/core/DialogContentText/index';
import FormControl from '@material-ui/core/FormControl/index';
import InputLabel from '@material-ui/core/InputLabel/index';
import Input from '@material-ui/core/Input/index';
import DialogActions from '@material-ui/core/DialogActions/index';
import Button from '@material-ui/core/Button/index';
import Dialog from '@material-ui/core/Dialog/index';
import React, { useState } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import validate from '../../../../validation/contactGroup/UpdateFormValidationRules';

const styles = () => ({
  validation: {
    color: 'red',
  },
}
);

function UpdateContactGroupModal({
  open, close, oldName, id, onSubmit, classes,
}) {
  const [name, setName] = useState(oldName);
  const [errors, setErrors] = useState({});

  const handleSubmit = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const err = validate(name);
    setErrors(err);
    if (Object.keys(err).length === 0) {
      onSubmit(id, name);
      close();
    }
  };

  return (
    <Dialog
      open={open}
      aria-labelledby="form-dialog-title"
      onClose={() => close()}
    >
      <DialogTitle id="form-dialog-title">Update contact group</DialogTitle>
      <DialogContent>
        <DialogContentText>
                    This form is use for updating contact group.
        </DialogContentText>
        <form
          onSubmit={handleSubmit}
        >
          <FormControl margin="normal" fullWidth>
            <InputLabel htmlFor="name">Contact group name</InputLabel>
            <Input
              id="name"
              onChange={(event) => {
                setName(event.target.value);
              }}
              name="name"
              value={name}
            />
            {errors.name && (
            <p className={classes.validation}>{errors.name}</p>
            )}
          </FormControl>
        </form>
      </DialogContent>
      <DialogActions>
        <Button color="primary" onClick={() => close()}>
                    Cancel
        </Button>
        <Button
          type="submit"
          color="primary"
          onClick={event => handleSubmit(event)}
        >
                    Update
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default withStyles(styles)(UpdateContactGroupModal);

