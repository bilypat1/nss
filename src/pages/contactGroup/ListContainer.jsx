import { connect } from 'react-redux';
import {
  selectLoggedUser,
  selectToken,
} from '../../resources/security';
import {
  addContactGroup,
  deleteContactGroup,
  editContactGroup,
  fetchPrivateGroups,
  fetchPublicGroups,
  selectPublicGroups,
  selectPrivateGroups,
} from '../../resources/contactGroup';
import Component from './ListComponent';

const mapStateToProps = (state) => {
  const loggedUser = selectLoggedUser(state);
  const group = selectPublicGroups(state);
  const token = selectToken(state);
  const privateGroup = selectPrivateGroups(state);

  return ({
    group: group.data,
    loggedUser: loggedUser.data,
    privateGroup: privateGroup.data,
    token: token.data,
  });
};

const mapDispatchToProps = dispatch => ({
  addContactGroup: (values, user) => dispatch(addContactGroup(values, user)),
  deleteContactGroup: (id, user) => dispatch(deleteContactGroup(id, user)),
  editContactGroup: (id, name, user) => dispatch(editContactGroup(id, name, user)),
  fetchPrivateGroups: id => dispatch(fetchPrivateGroups(id)),
  fetchPublicGroups: () => dispatch(fetchPublicGroups()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Component);
