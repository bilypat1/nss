import PropTypes from 'prop-types';
import React, {
  useEffect, useState,
} from 'react';
import { withRouter } from 'react-router';
import withStyles from '@material-ui/core/styles/withStyles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import Layout from '../../components/Layout/Layout';
import validate from '../../validation/contactGroup/CreateFormValidationRules';
import { UpdateContactGroupModal } from './components/UpdateContactGroupModal';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
    flexGrow: 1,
  },
  paper: {
    boxSizing: 'border-box',
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
    width: '100%',
    margin: '2.5rem 0',
    overflowX: 'scroll',
  },
  table: {
    minWidth: '320px',
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  button: {
    marginBottom: '2.5rem',
  },
  validation: {
    color: 'red',
  },
});

function ListContactGroups({
  classes, loggedUser, addContactGroup, token, fetchPublicGroups, fetchPrivateGroups, group, privateGroup, editContactGroup, deleteContactGroup,
}) {
  const [open, setOpen] = useState(false);
  const [updateContactGroup, setUpdateContactGroup] = useState(null);
  const [owned, setOwner] = useState(false);
  const [errors, setErrors] = useState({});
  const [values, setValues] = useState({});

  useEffect(() => {
    if (!group || !privateGroup) {
      fetchPublicGroups();
      fetchPrivateGroups(loggedUser.id);
    }
  });

  const groups = group || [];
  const privateGroups = privateGroup || [];

  const handleChange = (event) => {
    event.persist();
    setValues(values => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    e.stopPropagation();
    setErrors(validate(values));
    const error = validate(values);
    setErrors(error);
    if (Object.keys(error).length === 0) {
      if (owned === true) {
        addContactGroup(values, loggedUser.id, loggedUser.id).then(() => {
          setOpen(false);
          setOwner(false);
          fetchPrivateGroups(loggedUser.id);
          fetchPublicGroups();
        });
      } else {
        addContactGroup(values, null, loggedUser.id).then(() => {
          setOpen(false);
          setOwner(false);
          fetchPrivateGroups(loggedUser.id);
          fetchPublicGroups();
        });
      }
    }
  };

  const onUpdateOpen = (id, name) => {
    setUpdateContactGroup(<UpdateContactGroupModal id={id} oldName={name} open close={() => { setUpdateContactGroup(null); }} onSubmit={onUpdateGroupSubmit} />);
  };

  const onUpdateGroupSubmit = (id, name) => {
    editContactGroup(id, name, loggedUser.id);
  };
  const onDeteleGroup = (id) => {
    deleteContactGroup(id, loggedUser.id);
  };

  return (
    <Layout>
      <Button variant="contained" color="primary" className={classes.button} onClick={() => setOpen(true)}>
                Create new Contact Group
      </Button>
      <div>
        <Typography component="h1" variant="h5">
                    Public contacts groups
        </Typography>
        <Paper className={classes.paper}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {groups.map(row => (
                <TableRow key={row.id}>
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell align="right">
                    <Button onClick={() => { onUpdateOpen(row.id, row.name); }}>Update</Button>
                    <Button color="secondary" onClick={() => { onDeteleGroup(row.id); }}>Delete</Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
      </div>
      <div>
        <Typography component="h1" variant="h5">
                    Private contacts groups
        </Typography>
        <Paper className={classes.paper}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {privateGroups.map(row => (
                <TableRow key={row.id}>
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell align="right">
                    <Button onClick={() => { onUpdateOpen(row.id, row.name); }}>Update</Button>
                    <Button color="secondary" onClick={() => { onDeteleGroup(row.id); }}>Delete</Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
      </div>
      <Dialog
        open={open}
        aria-labelledby="form-dialog-title"
        onClose={() => setOpen(false)}
      >
        <DialogTitle id="form-dialog-title">Create new contact group</DialogTitle>
        <DialogContent>
          <DialogContentText>
                        This form is use for creating new contact group.
          </DialogContentText>
          <form
            onSubmit={handleSubmit}
          >
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="name">Contact group name</InputLabel>
              <Input
                id="name"
                className={`input ${errors.name && 'is-danger'}`}
                onChange={handleChange}
                value={values.name || ''}
                name="name"
                required
                autoFocus
              />
              {errors.name && (
                <p className={classes.validation}>{errors.name}</p>
              )}
            </FormControl>
            <FormControlLabel
              control={(
                <Checkbox
                  id="checkMe"
                  name="checkMe"
                  onChange={() => {
                    setOwner(!owned);
                  }}
                  value={owned}
                />
)}
              label="Private group"
            />
          </form>
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={() => setOpen(false)}>
                        Cancel
          </Button>
          <Button
            type="submit"
            color="primary"
            onClick={event => handleSubmit(event)}
          >
                        Create
          </Button>
        </DialogActions>

      </Dialog>
      {updateContactGroup}
    </Layout>
  );
}
ListContactGroups.propTypes = {
  addContactGroup: PropTypes.func.isRequired,
  fetchPublicGroups: PropTypes.func.isRequired,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      group: PropTypes.shape({
        title: PropTypes.string.isRequired,
      }).isRequired,
    }),
  ).isRequired,

};

export default withRouter(withStyles(styles)(ListContactGroups));
