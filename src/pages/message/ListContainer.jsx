import { connect } from 'react-redux';
import {
  selectLoggedUser,
  selectToken,
} from '../../resources/security';
import {
  fetchPrivateGroups,
  fetchPublicGroups,
  selectPublicGroups,
  selectPrivateGroups,
} from '../../resources/contactGroup';
import {
  fetchPrivateContacts,
  fetchPublicContacts,
  selectPrivateContacts,
  selectPublicContacts,
} from '../../resources/contact';
import {
  addMessage,
  fetchMessages,
  selectMessage,
} from '../../resources/messages';
import Component from './ListComponent';

const mapStateToProps = (state) => {
  const loggedUser = selectLoggedUser(state);
  const group = selectPublicGroups(state);
  const token = selectToken(state);
  const privateGroup = selectPrivateGroups(state);
  const publicContacs = selectPublicContacts(state);
  const privateContacs = selectPrivateContacts(state);
  const message = selectMessage(state);

  return ({
    group: group.data,
    loggedUser: loggedUser.data,
    messages: message.data,
    privateContacts: privateContacs.data,
    privateGroup: privateGroup.data,
    publicContacts: publicContacs.data,
    token: token.data,
  });
};

const mapDispatchToProps = dispatch => ({
  addMessages: (contactGroup, contact, content, actualUser) => dispatch(
    addMessage(contactGroup, contact, content, actualUser),
  ),
  fetchPrivateContacts: (id, token) => dispatch(fetchPrivateContacts(id, token)),
  fetchPrivateGroups: (token, id) => dispatch(fetchPrivateGroups(token, id)),
  fetchPrivateMessages: (user, token) => dispatch(fetchMessages(user, token)),
  fetchPublicContacts: token => dispatch(fetchPublicContacts(token)),
  fetchPublicGroups: token => dispatch(fetchPublicGroups(token)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Component);
