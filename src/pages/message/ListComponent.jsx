import { withRouter } from 'react-router';
import withStyles from '@material-ui/core/styles/withStyles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import React, {
  useEffect, useState,
} from 'react';
import Chip from '@material-ui/core/Chip';
import Layout from '../../components/Layout/Layout';
import { AddMessageModal } from './components/AddMessageModal';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
    flexGrow: 1,
  },
  paper: {
    boxSizing: 'border-box',
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
    width: '100%',
    margin: '2.5rem 0',
    overflowX: 'scroll',
  },
  table: {
    minWidth: '576px',
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  button: {
    marginBottom: '2.5rem',
  },
});

function ListMessagePage({
  classes, loggedUser, fetchPrivateMessages, messages, addMessages, token, fetchPublicGroups, fetchPrivateGroups, group, privateGroup, fetchPublicContacts, publicContacts, fetchPrivateContacts, privateContacts,
}) {
  useEffect(() => {
    if (!messages || !privateGroup || !publicContacts || !privateContacts || !privateGroups) {
      fetchPrivateMessages(loggedUser.id);
      fetchPublicGroups();
      fetchPrivateGroups(loggedUser.id);
      fetchPublicContacts();
      fetchPrivateContacts(loggedUser.id);
    }
  });

  const groups = group || [];
  const privateGroups = privateGroup || [];
  const publicContact = publicContacts || [];
  const privateContact = privateContacts || [];
  const [createMessage, setCreateMessage] = useState(null);
  if (!privateGroup || !groups || !publicContact || !privateContact) {
    return null;
  }

  const completeGroup = [...privateGroup, ...groups];
  const completeContact = [...publicContact, ...privateContact];

  const handleSubmit = (contactGroups, contacts, content) => {
    addMessages(contactGroups, contacts, content, loggedUser.id);
  };

  const onCreateOpen = () => {
    setCreateMessage(<AddMessageModal open close={() => { setCreateMessage(null); }} contactGroups={completeGroup} contacts={completeContact} onSubmit={handleSubmit} />);
  };

  const renderContacts = (contactId) => {
    const found = completeContact.find(contact => contact.id === contactId);

    if (!found) {
      return null;
    }

    return (
      <Chip
        className={classes.chip}
        label={found ? found.name : ''}
      />
    );
  };

  return (
    <Layout>
      <Button variant="contained" color="primary" className={classes.button} onClick={() => { onCreateOpen(); }}>
                Send new SMS
      </Button>
      <div>
        <Typography component="h1" variant="h5">
                    Sent messages
        </Typography>
        <Paper className={classes.paper}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell>Contacts</TableCell>
                <TableCell>Content</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {messages && Array.isArray(messages) && messages.map(row => (
                <TableRow key={row.id}>
                  <TableCell scope="row">
                    {row.contacts.map(renderContacts)}
                  </TableCell>
                  <TableCell scope="row">
                    {row.message}
                  </TableCell>
                </TableRow>
              ))}
              <TableRow />
            </TableBody>
          </Table>
        </Paper>
      </div>
      {createMessage}
    </Layout>
  );
}

export default withRouter(withStyles(styles)(ListMessagePage));
