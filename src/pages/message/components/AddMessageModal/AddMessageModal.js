import React, {useState} from 'react';
import Button from '@material-ui/core/Button/index';
import TextField from '@material-ui/core/TextField/index';
import Dialog from '@material-ui/core/Dialog/index';
import DialogActions from '@material-ui/core/DialogActions/index';
import DialogContent from '@material-ui/core/DialogContent/index';
import DialogTitle from '@material-ui/core/DialogTitle/index';
import FormControl from "@material-ui/core/FormControl/index";
import InputLabel from "@material-ui/core/InputLabel/index";
import Select from "@material-ui/core/Select/index";
import Input from "@material-ui/core/Input/index";
import MenuItem from "@material-ui/core/MenuItem/index";

function AddMessageModal({ open, close, contactGroups, contacts, onSubmit}) {
    const [newContactGroups, setNewContactGroups] = useState([]);
    const [newContacts, setNewContacts] = useState([]);
    const [message, setMessage] = useState("");

    const handleChangeGroups = event => {
        setNewContactGroups(event.target.value);
    };
    const handleChangeContacts = event => {
        setNewContacts(event.target.value);
    };
    const handleChangeMessage = event => {
        setMessage(event.target.value);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        e.stopPropagation();
        onSubmit(newContactGroups, newContacts, message);
        close();
    };

    return (
        <div>
            <Dialog
                open={open}
                onClose={() => close()}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Send new SMS</DialogTitle>
                <DialogContent>
                    <form
                        onSubmit={handleSubmit}
                    >
                        <FormControl margin="normal" required fullWidth>
                            <InputLabel htmlFor="select-multiple">Contacts</InputLabel>
                            <Select
                              fullWidth
                              multiple
                              value={newContactGroups}
                              onChange={handleChangeGroups}
                              input={<Input id="select-multiple" />}
                            >
                                {contactGroups.map(group => (
                                  <MenuItem key={group.name} value={group.id}>
                                      {group.name}
                                  </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                        <FormControl margin="normal" required fullWidth>
                            <InputLabel htmlFor="select-multiple">Contact grouos</InputLabel>
                            <Select
                              fullWidth
                              multiple
                              value={newContacts}
                              onChange={handleChangeContacts}
                              input={<Input id="select-multiple" />}
                            >
                                {contacts.map(group => (
                                  <MenuItem key={group.name} value={group.id}>
                                      {group.name}
                                  </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                        <FormControl margin="normal" required fullWidth>
                            <TextField
                              id="filled-multiline-flexible"
                              label="Message"
                              multiline
                              fullWidth
                              value={message}
                              rowsMax="4"
                              onChange={handleChangeMessage}
                              margin="normal"
                              variant="filled"
                            />
                        </FormControl>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button color="primary" onClick={() => close()}>
                        Cancel
                    </Button>
                    <Button  color="primary"  type="submit" onClick={(event)=>handleSubmit(event)}>
                        Send
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default AddMessageModal;