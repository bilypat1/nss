import { RSAA } from 'redux-api-middleware';
import { API_URL } from '../../config/parameters';
import * as actionTypes from './actionTypes';

export const fetchPublicContacts = () => ({
  [RSAA]: {
    endpoint: `${API_URL}/contacts/public`,
    method: 'GET',
    types: [
      actionTypes.FETCH_PUBLIC_CONTACTS_LIST_REQUEST,
      actionTypes.FETCH_PUBLIC_CONTACTS_LIST_SUCCESS,
      actionTypes.FETCH_PUBLIC_CONTACTS_LIST_FAILURE,
    ],
  },
});

export const fetchPrivateContacts = id => ({
  [RSAA]: {
    endpoint: `${API_URL}/users/${id}/contacts`,
    method: 'GET',
    types: [
      actionTypes.FETCH_PRIVATE_CONTACTS_LIST_REQUEST,
      actionTypes.FETCH_PRIVATE_CONTACTS_LIST_SUCCESS,
      actionTypes.FETCH_PRIVATE_CONTACTS_LIST_FAILURE,
    ],
  },
});

export const addContact = (
  values,
  contactGroups,
  user,
  actualUser,
) => dispatch => dispatch({
  [RSAA]: {
    body: user
      ? JSON.stringify({
        contactGroups,
        name: values.name,
        phoneNumber: values.phoneNumber,
        user,
      })
      : JSON.stringify({
        contactGroups,
        name: values.name,
        phoneNumber: values.phoneNumber,
      }),
    endpoint: `${API_URL}/contacts`,
    method: 'POST',
    types: [
      actionTypes.ADD_CONTACT_REQUEST,
      actionTypes.ADD_CONTACT_SUCCESS,
      actionTypes.ADD_CONTACT_FAILURE,
    ],
  },
}).then((response) => {
  if (response.type === actionTypes.ADD_CONTACT_SUCCESS) {
    dispatch(fetchPrivateContacts(actualUser));
    dispatch(fetchPublicContacts());
  }

  return response;
});

export const editContact = (
  id,
  values,
  actualUser,
) => dispatch => dispatch({
  [RSAA]: {
    body: JSON.stringify({
      name: values.name,
      phoneNumber: values.phoneNumber,
    }),
    endpoint: `${API_URL}/contacts/${id}`,
    method: 'PUT',
    types: [
      actionTypes.EDIT_CONTACT_REQUEST,
      actionTypes.EDIT_CONTACT_SUCCESS,
      actionTypes.EDIT_CONTACT_FAILURE,
    ],
  },
}).then((response) => {
  if (response.type === actionTypes.EDIT_CONTACT_SUCCESS) {
    dispatch(fetchPrivateContacts(actualUser));
    dispatch(fetchPublicContacts());
  }

  return response;
});

export const deleteContact = (id, actualUser) => dispatch => dispatch({
  [RSAA]: {
    endpoint: `${API_URL}/contacts/${id}`,
    method: 'DELETE',
    types: [
      actionTypes.DELETE_CONTACT_REQUEST,
      actionTypes.DELETE_CONTACT_SUCCESS,
      actionTypes.DELETE_CONTACT_FAILURE,
    ],
  },
}).then((response) => {
  if (response.type === actionTypes.DELETE_CONTACT_SUCCESS) {
    dispatch(fetchPrivateContacts(actualUser));
    dispatch(fetchPublicContacts());
  }

  return response;
});
