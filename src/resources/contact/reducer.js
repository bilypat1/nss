import * as Immutable from 'immutable';
import initialState from './initialState';
import * as actionTypes from './actionTypes';

export default (state, action) => {
  if (typeof state === 'undefined') {
    return initialState;
  }

  const {
    type, payload,
  } = action;

  if (type === actionTypes.FETCH_PUBLIC_CONTACTS_LIST_REQUEST) {
    return state
      .setIn(['publicContacts', 'isFetched'], false)
      .setIn(['publicContacts', 'isFetching'], true)
      .setIn(['publicContacts', 'isFetchingFailure'], false);
  }

  if (type === actionTypes.FETCH_PUBLIC_CONTACTS_LIST_SUCCESS) {
    return state
      .setIn(['publicContacts', 'isFetched'], true)
      .setIn(['publicContacts', 'isFetching'], false)
      .setIn(['publicContacts', 'isFetchingFailure'], false)
      .setIn(['publicContacts', 'data'], Immutable.fromJS(payload));
  }

  if (type === actionTypes.FETCH_PUBLIC_CONTACTS_LIST_FAILURE) {
    return state
      .setIn(['publicContacts', 'isFetched'], true)
      .setIn(['publicContacts', 'isFetching'], false)
      .setIn(['publicContacts', 'isFetchingFailure'], true);
  }

  if (type === actionTypes.FETCH_PRIVATE_CONTACTS_LIST_REQUEST) {
    return state
      .setIn(['privateContacts', 'isFetched'], false)
      .setIn(['privateContacts', 'isFetching'], true)
      .setIn(['privateContacts', 'isFetchingFailure'], false);
  }

  if (type === actionTypes.FETCH_PRIVATE_CONTACTS_LIST_SUCCESS) {
    return state
      .setIn(['privateContacts', 'isFetched'], true)
      .setIn(['privateContacts', 'isFetching'], false)
      .setIn(['privateContacts', 'isFetchingFailure'], false)
      .setIn(['privateContacts', 'data'], Immutable.fromJS(payload));
  }

  if (type === actionTypes.FETCH_PRIVATE_CONTACTS_LIST_FAILURE) {
    return state
      .setIn(['privateContacts', 'isFetched'], true)
      .setIn(['privateContacts', 'isFetching'], false)
      .setIn(['privateContacts', 'isFetchingFailure'], true);
  }

  return state;
};
