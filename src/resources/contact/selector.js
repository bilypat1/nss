import { createSelector } from 'reselect';

const getPublicContacts = state => state.getIn(['contact', 'publicContacts']);
const getPrivateContacts = state => state.getIn(['contact', 'privateContacts']);

export const selectPublicContacts = createSelector(
  [getPublicContacts],
  data => data.toJS(),
);

export const selectPrivateContacts = createSelector(
  [getPrivateContacts],
  data => data.toJS(),
);
