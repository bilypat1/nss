import * as Immutable from 'immutable';
import initialState from './initialState';
import * as actionTypes from './actionTypes';

export default (state, action) => {
  if (typeof state === 'undefined') {
    return initialState;
  }

  const {
    type, payload,
  } = action;

  if (type === actionTypes.FETCH_PUBLIC_GROUP_LIST_REQUEST) {
    return state
      .setIn(['contactGroup', 'isFetched'], false)
      .setIn(['group', 'isFetching'], true)
      .setIn(['group', 'isFetchingFailure'], false);
  }

  if (type === actionTypes.FETCH_PUBLIC_GROUP_LIST_SUCCESS) {
    return state
      .setIn(['group', 'isFetched'], true)
      .setIn(['group', 'isFetching'], false)
      .setIn(['group', 'isFetchingFailure'], false)
      .setIn(['group', 'data'], Immutable.fromJS(payload));
  }

  if (type === actionTypes.FETCH_PUBLIC_GROUP_LIST_FAILURE) {
    return state
      .setIn(['loggedUser', 'isFetched'], true)
      .setIn(['group', 'isFetching'], false)
      .setIn(['group', 'isFetchingFailure'], true);
  }

  if (type === actionTypes.FETCH_PRIVATE_GROUP_LIST_REQUEST) {
    return state
      .setIn(['privateGroup', 'isFetched'], false)
      .setIn(['privateGroup', 'isFetching'], true)
      .setIn(['privateGroup', 'isFetchingFailure'], false);
  }

  if (type === actionTypes.FETCH_PRIVATE_GROUP_LIST_SUCCESS) {
    return state
      .setIn(['privateGroup', 'isFetched'], true)
      .setIn(['privateGroup', 'isFetching'], false)
      .setIn(['privateGroup', 'isFetchingFailure'], false)
      .setIn(['privateGroup', 'data'], Immutable.fromJS(payload));
  }

  if (type === actionTypes.FETCH_PRIVATE_GROUP_LIST_FAILURE) {
    return state
      .setIn(['privateGroup', 'isFetched'], true)
      .setIn(['privateGroup', 'isFetching'], false)
      .setIn(['privateGroup', 'isFetchingFailure'], true);
  }

  return state;
};
