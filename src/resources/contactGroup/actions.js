import { RSAA } from 'redux-api-middleware';
import { API_URL } from '../../config/parameters';
import * as actionTypes from './actionTypes';

export const fetchPublicGroups = () => ({
  [RSAA]: {
    endpoint: `${API_URL}/contact-groups/public`,
    method: 'GET',
    types: [
      actionTypes.FETCH_PUBLIC_GROUP_LIST_REQUEST,
      actionTypes.FETCH_PUBLIC_GROUP_LIST_SUCCESS,
      actionTypes.FETCH_PUBLIC_GROUP_LIST_FAILURE,
    ],
  },
});

export const fetchPrivateGroups = id => ({
  [RSAA]: {
    endpoint: `${API_URL}/users/${id}/contact-groups`,
    method: 'GET',
    types: [
      actionTypes.FETCH_PRIVATE_GROUP_LIST_REQUEST,
      actionTypes.FETCH_PRIVATE_GROUP_LIST_SUCCESS,
      actionTypes.FETCH_PRIVATE_GROUP_LIST_FAILURE,
    ],
  },
});

export const addContactGroup = (values, user, actualUser) => dispatch => dispatch({
  [RSAA]: {
    body: user
      ? JSON.stringify({
        name: values.name,
        user,
      })
      : JSON.stringify({ name: values.name }),
    endpoint: `${API_URL}/contact-groups`,
    method: 'POST',
    types: [
      actionTypes.ADD_CONTACT_GROUP_REQUEST,
      actionTypes.ADD_CONTACT_GROUP_SUCCESS,
      actionTypes.ADD_CONTACT_GROUP_FAILURE,
    ],
  },
}).then((response) => {
  if (response.type === actionTypes.ADD_CONTACT_GROUP_SUCCESS) {
    dispatch(fetchPrivateGroups(actualUser));
    dispatch(fetchPublicGroups());
  }

  return response;
});

export const editContactGroup = (id, name, actualUser) => dispatch => dispatch({
  [RSAA]: {
    body: JSON.stringify({ name }),
    endpoint: `${API_URL}/contact-groups/${id}`,
    method: 'PUT',
    types: [
      actionTypes.EDIT_CONTACT_GROUP_REQUEST,
      actionTypes.EDIT_CONTACT_GROUP_SUCCESS,
      actionTypes.EDIT_CONTACT_GROUP_FAILURE,
    ],
  },
}).then((response) => {
  if (response.type === actionTypes.EDIT_CONTACT_GROUP_SUCCESS) {
    dispatch(fetchPrivateGroups(actualUser));
    dispatch(fetchPublicGroups());
  }

  return response;
});

export const deleteContactGroup = (id, actualUser) => dispatch => dispatch({
  [RSAA]: {
    endpoint: `${API_URL}/contact-groups/${id}`,
    method: 'DELETE',
    types: [
      actionTypes.DELETE_CONTACT_GROUP_REQUEST,
      actionTypes.DELETE_CONTACT_GROUP_SUCCESS,
      actionTypes.DELETE_CONTACT_GROUP_FAILURE,
    ],
  },
}).then((response) => {
  if (response.type === actionTypes.DELETE_CONTACT_GROUP_SUCCESS) {
    dispatch(fetchPrivateGroups(actualUser));
    dispatch(fetchPublicGroups());
  }

  return response;
});
