import { createSelector } from 'reselect';

const getPublicGroups = state => state.getIn(['contactGroup', 'group']);
const getPrivateGroups = state => state.getIn(['contactGroup', 'privateGroup']);

export const selectPublicGroups = createSelector(
  [getPublicGroups],
  data => data.toJS(),
);
export const selectPrivateGroups = createSelector(
  [getPrivateGroups],
  data => data.toJS(),
);
