import * as Immutable from 'immutable';

export default Immutable.fromJS({
  publicUser: {
    data: null,
    isFetching: false,
    isFetchingFailure: false,
    isPosting: false,
    isPostingFailure: false,
  },
});
