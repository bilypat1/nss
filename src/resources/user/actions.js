import { RSAA } from 'redux-api-middleware';
import { API_URL } from '../../config/parameters';
import * as actionTypes from './actionTypes';

export const fetchPublicUsers = () => ({
  [RSAA]: {
    endpoint: `${API_URL}/users`,
    method: 'GET',
    types: [
      actionTypes.FETCH_PUBLIC_USERS_LIST_REQUEST,
      actionTypes.FETCH_PUBLIC_USERS_LIST_SUCCESS,
      actionTypes.FETCH_PUBLIC_USERS_LIST_FAILURE,
    ],
  },
});

export const addUser = (values, role) => dispatch => dispatch({
  [RSAA]: {
    body: JSON.stringify({
      email: values.email,
      firstName: values.firstName,
      lastName: values.lastName,
      password: values.password,
      role,
    }),
    endpoint: `${API_URL}/users`,
    method: 'POST',
    types: [
      actionTypes.ADD_USER_REQUEST,
      actionTypes.ADD_USER_SUCCESS,
      actionTypes.ADD_USER_FAILURE,
    ],
  },
}).then((response) => {
  if (response.type === actionTypes.ADD_USER_SUCCESS) {
    dispatch(fetchPublicUsers());
  }
  return response;
});

export const editUser = (id, values, role) => dispatch => dispatch({
  [RSAA]: {
    body: JSON.stringify({
      email: values.email,
      firstName: values.firstName,
      lastName: values.lastName,
      role,
    }),
    endpoint: `${API_URL}/users/${id}`,
    method: 'PUT',
    types: [
      actionTypes.EDIT_USER_REQUEST,
      actionTypes.EDIT_USER_SUCCESS,
      actionTypes.EDIT_USER_FAILURE,
    ],
  },
}).then((response) => {
  if (response.type === actionTypes.EDIT_USER_SUCCESS) {
    dispatch(fetchPublicUsers());
  }

  return response;
});

export const deleteUser = id => dispatch => dispatch({
  [RSAA]: {
    endpoint: `${API_URL}/users/${id}`,
    method: 'DELETE',
    types: [
      actionTypes.DELETE_USER_REQUEST,
      actionTypes.DELETE_USER_SUCCESS,
      actionTypes.DELETE_USER_FAILURE,
    ],
  },
}).then((response) => {
  if (response.type === actionTypes.DELETE_USER_SUCCESS) {
    dispatch(fetchPublicUsers());
  }

  return response;
});
