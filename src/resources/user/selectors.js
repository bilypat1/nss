import { createSelector } from 'reselect';

const getPublicUsers = state => state.getIn(['user', 'publicUser']);

export const selectPublicUser = createSelector(
  [getPublicUsers],
  data => data.toJS(),
);
