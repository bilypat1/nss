import * as Immutable from 'immutable';
import initialState from './initialState';
import * as actionTypes from './actionTypes';

export default (state, action) => {
  if (typeof state === 'undefined') {
    return initialState;
  }

  const {
    type,
    payload,
  } = action;

  if (type === actionTypes.FETCH_PUBLIC_USERS_LIST_REQUEST) {
    return state
      .setIn(['publicUser', 'isFetched'], false)
      .setIn(['publicUser', 'isFetching'], true)
      .setIn(['publicUser', 'isFetchingFailure'], false);
  }

  if (type === actionTypes.FETCH_PUBLIC_USERS_LIST_SUCCESS) {
    return state
      .setIn(['publicUser', 'isFetched'], true)
      .setIn(['publicUser', 'isFetching'], false)
      .setIn(['publicUser', 'isFetchingFailure'], false)
      .setIn(['publicUser', 'data'], Immutable.fromJS(payload));
  }

  if (type === actionTypes.FETCH_PUBLIC_USERS_LIST_FAILURE) {
    return state
      .setIn(['publicUser', 'isFetched'], true)
      .setIn(['publicUser', 'isFetching'], false)
      .setIn(['publicUser', 'isFetchingFailure'], true);
  }

  return state;
};
