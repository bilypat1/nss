import { createSelector } from 'reselect';

const getMessage = state => state.getIn(['message', 'messages']);

export const selectMessage = createSelector(
  [getMessage],
  data => data.toJS(),
);
