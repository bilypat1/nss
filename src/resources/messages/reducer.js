import * as Immutable from 'immutable';
import initialState from './initialState';
import * as actionTypes from './actionTypes';

export default (state, action) => {
  if (typeof state === 'undefined') {
    return initialState;
  }

  const {
    type, payload,
  } = action;

  if (type === actionTypes.FETCH_MESSAGE_LIST_REQUEST) {
    return state
      .setIn(['messages', 'isFetched'], false)
      .setIn(['messages', 'isFetching'], true)
      .setIn(['messages', 'isFetchingFailure'], false);
  }

  if (type === actionTypes.FETCH_MESSAGE_LIST_SUCCESS) {
    return state
      .setIn(['messages', 'isFetched'], true)
      .setIn(['messages', 'isFetching'], false)
      .setIn(['messages', 'isFetchingFailure'], false)
      .setIn(['messages', 'data'], Immutable.fromJS(payload));
  }

  if (type === actionTypes.FETCH_MESSAGE_LIST_FAILURE) {
    return state
      .setIn(['message', 'isFetched'], true)
      .setIn(['messages', 'isFetching'], false)
      .setIn(['messages', 'isFetchingFailure'], true);
  }

  if (type === actionTypes.ADD_MESSAGE_FAILURE) {
    return state
      .setIn(['message', 'isPosting'], true)
      .setIn(['message', 'isPostingFailure'], false);
  }

  if (type === actionTypes.ADD_MESSAGE_SUCCESS) {
    return state
      .setIn(['messages', 'isPosting'], false)
      .setIn(['messages', 'isPostingFailure'], false)
      .setIn(['messages', 'data'], Immutable.fromJS(payload));
  }

  if (type === actionTypes.ADD_MESSAGE_FAILURE) {
    return state
      .setIn(['message', 'isPosting'], false)
      .setIn(['message', 'isPostingFailure'], true);
  }

  return state;
};
