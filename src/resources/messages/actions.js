import { RSAA } from 'redux-api-middleware';
import { API_URL } from '../../config/parameters';
import * as actionTypes from './actionTypes';

export const fetchMessages = id => ({
  [RSAA]: {
    endpoint: `${API_URL}/users/${id}/messages`,
    method: 'GET',
    types: [
      actionTypes.FETCH_MESSAGE_LIST_REQUEST,
      actionTypes.FETCH_MESSAGE_LIST_SUCCESS,
      actionTypes.FETCH_MESSAGE_LIST_FAILURE,
    ],
  },
});

export const addMessage = (contactGroup, contact, message, actualUser) => dispatch => dispatch({
  [RSAA]: {
    body: JSON.stringify({
      contactGroups: contactGroup,
      contacts: contact,
      message,
    }),
    endpoint: `${API_URL}/messages`,
    method: 'POST',
    types: [
      actionTypes.ADD_MESSAGE_REQUEST,
      actionTypes.ADD_MESSAGE_SUCCESS,
      actionTypes.ADD_MESSAGE_FAILURE,
    ],
  },
}).then((response) => {
  if (response.type === actionTypes.ADD_MESSAGE_SUCCESS) {
    dispatch(fetchMessages(actualUser));
  }

  return response;
});
