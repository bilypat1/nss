import * as Immutable from 'immutable';

export default Immutable.fromJS({
  messages: {
    data: null,
    isFetched: false,
    isFetching: false,
    isFetchingFailure: false,
    isPosting: false,
    isPostingFailure: false,
  },
});
