export const ADD_MESSAGE_REQUEST = 'message/ADD_MESSAGE_REQUEST';
export const ADD_MESSAGE_SUCCESS = 'message/ADD_MESSAGE_SUCCESS';
export const ADD_MESSAGE_FAILURE = 'message/ADD_MESSAGE_FAILURE';

export const FETCH_MESSAGE_LIST_REQUEST = 'message/FETCH_MESSAGE_LIST_REQUEST';
export const FETCH_MESSAGE_LIST_SUCCESS = 'message/FETCH_MESSAGE_LIST_SUCCESS';
export const FETCH_MESSAGE_LIST_FAILURE = 'message/FETCH_MESSAGE_LIST_FAILURE';