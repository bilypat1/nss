import * as Immutable from 'immutable';
import initialState from './initialState';
import * as actionTypes from './actionTypes';

export default (state, action) => {
  if (typeof state === 'undefined') {
    return initialState;
  }

  const {
    payload,
    type,
  } = action;

  if (type === actionTypes.FETCH_LOGGED_USER_REQUEST) {
    return state
      .setIn(['loggedUser', 'isFetched'], false)
      .setIn(['loggedUser', 'isFetching'], true)
      .setIn(['loggedUser', 'isFetchingFailure'], false);
  }

  if (type === actionTypes.FETCH_LOGGED_USER_SUCCESS) {
    const { email } = state.toJS().loggedUser.data;
    const { token } = state.toJS().loggedUser.data;
    return state
      .setIn(['loggedUser', 'isFetched'], true)
      .setIn(['loggedUser', 'isFetching'], false)
      .setIn(['loggedUser', 'isFetchingFailure'], false)
      .setIn(['token', 'data'], token)
      .setIn(['loggedUser', 'data'], Immutable.fromJS(payload.filter(user => user.email === email)[0]));
  }

  if (type === actionTypes.FETCH_LOGGED_USER_FAILURE) {
    return state
      .setIn(['loggedUser', 'isFetched'], true)
      .setIn(['loggedUser', 'isFetching'], false)
      .setIn(['loggedUser', 'isFetchingFailure'], true);
  }

  if (type === actionTypes.LOGIN_REQUEST) {
    return state
      .setIn(['loggedUser', 'isPosting'], true)
      .setIn(['loggedUser', 'isPostingFailure'], false);
  }

  if (type === actionTypes.LOGIN_SUCCESS) {
    const token = Immutable.fromJS(payload.token);
    return state
      .setIn(['loggedUser', 'isPosting'], false)
      .setIn(['loggedUser', 'isPostingFailure'], false)
      .setIn(['token', 'data'], token)
      .setIn(['loggedUser', 'data'], Immutable.fromJS(payload));
  }

  if (type === actionTypes.LOGIN_FAILURE) {
    return state
      .setIn(['loggedUser', 'isPosting'], false)
      .setIn(['loggedUser', 'isPostingFailure'], true);
  }

  if (type === actionTypes.LOGOUT_REQUEST) {
    return state
      .setIn(['loggedUser', 'isPosting'], true)
      .setIn(['loggedUser', 'isPostingFailure'], false);
  }

  if (type === actionTypes.LOGOUT_SUCCESS) {
    return state
      .setIn(['loggedUser', 'isPosting'], false)
      .setIn(['loggedUser', 'isPostingFailure'], false)
      .setIn(['loggedUser', 'data'], null);
  }

  if (type === actionTypes.LOGOUT_FAILURE) {
    return state
      .setIn(['loggedUser', 'isPosting'], false)
      .setIn(['loggedUser', 'isPostingFailure'], true);
  }

  if (type === actionTypes.USER_TOKEN_SUCCESS) {
    return state
      .setIn(['token', 'data'], Immutable.fromJS(payload));
  }

  if (type === actionTypes.USER_EMAIL_SUCCESS) {
    return state
      .setIn(['loggedUser', 'data', 'email'], Immutable.fromJS(payload));
  }

  return state;
};
