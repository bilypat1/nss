import * as Immutable from 'immutable';

export default Immutable.fromJS({
  loggedUser: {
    data: null,
    isFetched: false,
    isFetching: false,
    isFetchingFailure: false,
    isPosting: false,
    isPostingFailure: false,
    token: null,
  },
  token: {
    data: null,
    isFetched: false,
    isFetching: false,
    isFetchingFailure: false,
    isPosting: false,
    isPostingFailure: false,
  },
});
