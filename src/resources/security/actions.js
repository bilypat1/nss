import { RSAA } from 'redux-api-middleware';
import { API_URL } from '../../config/parameters';
import history from '../../history';
import * as actionTypes from './actionTypes';

export const getCurrentUser = () => ({
  [RSAA]: {
    endpoint: `${API_URL}/users`,
    method: 'GET',
    types: [
      actionTypes.FETCH_LOGGED_USER_REQUEST,
      actionTypes.FETCH_LOGGED_USER_SUCCESS,
      actionTypes.FETCH_LOGGED_USER_FAILURE,
    ],
  },
});

export const setToken = token => ({
  payload: token,
  type: actionTypes.USER_TOKEN_SUCCESS,
});

export const setEmail = email => ({
  payload: email,
  type: actionTypes.USER_EMAIL_SUCCESS,
});

export const logInUser = (values) => dispatch => dispatch({
  [RSAA]: {
    body: JSON.stringify({
      email: values.email,
      password: values.password,
    }),
    endpoint: `${API_URL}/auth/login`,
    method: 'POST',
    types: [
      actionTypes.LOGIN_REQUEST,
      actionTypes.LOGIN_SUCCESS,
      actionTypes.LOGIN_FAILURE,
    ],
  },

}).then((response) => {
  if (response.type === actionTypes.LOGIN_SUCCESS) {
    dispatch(getCurrentUser(response.payload.token));
    dispatch(setToken(response.payload.token));
    dispatch(setEmail(values.email));

    history.push('/message');
  }

  return response;
});
