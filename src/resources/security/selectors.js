import { createSelector } from 'reselect';

const getLoggedUser = state => state.getIn(['security', 'loggedUser']);
const getToken = state => state.getIn(['security', 'token']);

export const selectLoggedUser = createSelector(
  [getLoggedUser],
  data => data.toJS(),
);
export const selectToken = createSelector(
  [getToken],
  data => data.toJS(),
);
