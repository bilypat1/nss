export const FETCH_LOGGED_USER_REQUEST = 'security/FETCH_LOGGED_USER_REQUEST';
export const FETCH_LOGGED_USER_SUCCESS = 'security/FETCH_LOGGED_USER_SUCCESS';
export const FETCH_LOGGED_USER_FAILURE = 'security/FETCH_LOGGED_USER_FAILURE';

export const LOGIN_REQUEST = 'security/LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'security/LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'security/LOGIN_FAILURE';

export const LOGOUT_REQUEST = 'security/LOGOUT_REQUEST';
export const LOGOUT_SUCCESS = 'security/LOGOUT_SUCCESS';
export const LOGOUT_FAILURE = 'security/LOGOUT_FAILURE';

export const USER_TOKEN_SUCCESS = 'security/USER_TOKEN';
export const USER_EMAIL_SUCCESS = 'security/USER_EMAIL';

