import React from 'react';
import { Provider } from 'react-redux';
import {
  Router,
  Route,
  Switch,
} from 'react-router-dom';
import history from './history';
import store from './store';
import './App.css';
import { AuthorizedRoute } from './components/AuthorizedRoute';
import SignIn from './pages/security/LoginPage';
import { ListPage as ContactListPage } from './pages/contact';
import { ListPage as ContactGroupListPage } from './pages/contactGroup';
import { ListPage as MessageListPage } from './pages/message';
import { ListPage as UserListPage } from './pages/user';

const App = () => (
  <Provider store={store}>
    <Router history={history} onUpdate={() => window.scrollTo(0, 0)}>
      <Switch>
        <Route exact path="/" component={SignIn} />
        <AuthorizedRoute exact path="/contacts" component={ContactListPage} />
        <AuthorizedRoute exact path="/contactGroup" component={ContactGroupListPage} />
        <AuthorizedRoute exact path="/message" component={MessageListPage} />
        <AuthorizedRoute exact path="/users" component={UserListPage} />
      </Switch>
    </Router>
  </Provider>
);

export default App;
