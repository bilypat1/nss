import { STORE_KEY } from '../config/parameters';

export const getApplicationStore = () => {
  const applicationStore = localStorage.getItem(STORE_KEY);

  if (!applicationStore) {
    return null;
  }

  try {
    return JSON.parse(applicationStore);
  } catch (e) {
    return null;
  }
};

export const setApplicationStore = (applicationStore) => {
  localStorage.setItem(STORE_KEY, JSON.stringify(applicationStore));
};

export const removeApplicationStore = () => {
  localStorage.removeItem(STORE_KEY);
};
