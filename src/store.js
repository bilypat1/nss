import Immutable from 'immutable';
import { apiMiddleware } from 'redux-api-middleware';
import thunkMiddleware from 'redux-thunk';
import {
  applyMiddleware,
  createStore,
} from 'redux';
import apiConfigurationMiddleware from './middlewares/apiConfigurationMiddleware';
import apiError404Middleware from './middlewares/apiError404Middleware';
import {
  getApplicationStore,
  setApplicationStore,
} from './services/storageService';
import reducer from './reducer';

const middlewares = [
  thunkMiddleware,
  apiConfigurationMiddleware,
  apiMiddleware,
  apiError404Middleware,
];

const savedApplicationStore = getApplicationStore();
let store;

if (savedApplicationStore) {
  store = createStore(
    reducer,
    Immutable.fromJS(savedApplicationStore),
    applyMiddleware(...middlewares),
  );
} else {
  store = createStore(
    reducer,
    applyMiddleware(...middlewares),
  );
}

store.subscribe(() => {
  setApplicationStore(store.getState());
});

export default store;
