import history from '../history';

const apiError404Middleware = () => next => (action) => {
  if (typeof action.payload !== 'undefined' && action.payload && action.payload.status === 404) {
    history.push('/404/');

    return false;
  }

  return next(action);
};

export default apiError404Middleware;
