import { RSAA } from 'redux-api-middleware';
import { selectToken } from '../resources/security';

const apiConfigurationMiddleware = store => next => (action) => {
  const callApi = action[RSAA];
  const token = selectToken(store.getState());

  if (callApi) {
    callApi.headers = Object.assign({}, callApi.headers, {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    });

    if (token && token.data) {
      callApi.headers = Object.assign({}, callApi.headers, {
        Authorization: `Bearer ${token.data}`,
      });
    }

    return next(Object.assign({}, action, { [RSAA]: callApi }));
  }

  return next(action);
};

export default apiConfigurationMiddleware;
