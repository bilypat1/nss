import { combineReducers } from 'redux-immutable';
import { reducer as securityReducer } from './resources/security';
import { reducer as contactGroupReducer } from './resources/contactGroup';
import { reducer as contactReducer } from './resources/contact';
import { reducer as userReducer } from './resources/user';
import { reducer as messageReducer } from './resources/messages';

const appReducers = combineReducers({
  contact: contactReducer,
  contactGroup: contactGroupReducer,
  message: messageReducer,
  security: securityReducer,
  user: userReducer,
});

export default (state, action) => appReducers(state, action);
